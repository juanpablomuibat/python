# SISTEMA DE REGISTRO DE ASISTENCIA BIOMETRICO
Se creo un registro de asistencia biométrico, desarrollado con el lenguaje de programación Python 

![Employee data](image/funcion.gif)
## Descripción del Proyecto


Este sistema de registro proporciona una solución efectiva para la marcación precisa de la hora de entrada y salida en distintos lugares, siendo especialmente aplicado en entornos laborales para registrar el horario de llegada de los trabajadores de manera eficiente.


## Configuración y Ejecución del Proyecto


## Librerías Utilizadas



## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado "Programa y Libera tu Potencial" ha desempeñado un papel fundamental como nuestra guía principal, brindándonos una base sólida en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la potenciación de la creatividad a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Deseamos expresar nuestro más sincero agradecimiento al docente, al decano de la facultad de ingeniería y a la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su invaluable apoyo y orientación a lo largo de este proyecto. Su compromiso y respaldo han sido fundamentales para el éxito de nuestro trabajo.
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UPDS](https://www.facebook.com/UPDS.bo)
Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)
Docente:
- [PhD.  JAIME ZAMBRANA CHACÓN](https://facebook.com/zambranachaconjaime)
Equipo de desarrollo:
- Pablo Antonio Mamani Gonzales
- Juan Pablo Muiba Triguero
- Pablo Moreno Terrazas
- Michael Abraham Castro Torrez