# Juego: Ahorcado 

Ahorcado, es un juego en Python que reta a adivinar palabras al azar en un máximo de 7 intentos

![Employee data](image/imagenproy.gif)
## Descripción del Proyecto
Se presentó un juego llamado 'Ahorcado', programado en Python. Para su desarrollo se utilizaron las bibliotecas 'random' y 'string', así como conceptos fundamentales de programación en Python como funciones, sentencias repetitivas, decisiones y variables. El objetivo del juego es adivinar palabras al azar escogiendo letras del abecedario, con un límite de 7 intentos por palabra.

## Configuración y Ejecución del Proyecto

Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
- Asegúrate de tener Python 3.11 instalado.
- Ejecuta el archivo ahorcado.py para iniciar el juego
## Librerías Utilizadas

- [random](https://docs.python.org/3/library/random.html)

El módulo random de Python es muy útil para generar números aleatorios. Aquí te dejo algunas de las funciones más importantes:

- random.random(): Devuelve un número flotante aleatorio entre 0.0 y 1.0.

- random.randint(a, b): Devuelve un número entero aleatorio entre a y b (ambos incluidos).

- random.uniform(a, b): Devuelve un número flotante aleatorio entre a y b (o b y a), que pueden ser tanto positivos como negativos.

- random.choice(seq): Devuelve un elemento aleatorio de la secuencia no vacía seq. Si seq está vacío, lanza un IndexError.

- random.shuffle(x[, random]): Mezcla la secuencia x en su lugar, utilizando solo resultados de random() como fuente de aleatoriedad.

- random.seed(a=None, version=2): Inicializa el generador de números aleatorios. Si omite a, el sistema intentará usar una fuente más aleatoria disponible.

Estos son solo algunos ejemplos de las funciones disponibles en el módulo random.  


- [string](https://docs.python.org/3/library/stdtypes.html#string-methods)

Aquí encontrarás algunos ejemplos de cómo puedes utilizar los métodos de cadena en Python.


- ```str.ascii_uppercase``` es una constante predefinida en Python que contiene una cadena de todos los caracteres ASCII en mayúsculas. La cadena es 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

- El método split() divide una cadena en una lista donde cada palabra es un elemento de la lista.

``` bash python
abecedario = set(string.ascii_uppercase)
```


## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado 'Programa y Libera tu Potencial' ha sido nuestra guía fundamental, proporcionándonos un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la liberación del potencial creativo a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Queremos agradecer a nuestro docente, al decano de la facultad de ingeniería y a la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su apoyo y orientación a lo largo de este proyecto.
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UNIVERSIDAD PRIVADA DOMINGO SAVIO](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)
Docente:
- [PhD. Jaime Zambrana Chacón](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- Jonathan River Arrieta Cortez
- Juan Jose Flores Manaca
- Lider Limbert Machaca Estrada
- Carla Vásquez Téllez

