from tkinter import *
import numpy as np
import cv2
from PIL import Image, ImageTk
  

#
ventana = Tk()
ventana.title("Abrir OpenCV con Python")
ventana.geometry("800x600")



cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 500)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 500)
ventana.bind('', lambda e: ventana.quit())

def show_frame():
    ret, frame = cap.read()
	cv2image = cv2.cvtColor(frame,cv2.COLOR_RGB2BGR)

	img   = Image.fromarray(cv2image).resize((760, 400))
	imgtk = ImageTk.PhotoImage(image = img)
	lmain.imgtk = imgtk
	lmain.configure(image=imgtk)
	lmain.after(10, show_frame)

show_frame()  #Display    





ventana.mainloop()



# # Create a window
# ventana = Tk()

# # Load an image using OpenCV
# cv_img = cv2.imread("https://i.ytimg.com/vi/PTGOxqKyE4M/maxresdefault.jpg")

# # Get the image dimensions (OpenCV stores image data as NumPy ndarray)
# height, width, no_channels = cv_img.shape
  
#  # Create a canvas that can fit the above image
# canvas = Tk.Canvas(ventana, width = width, height = height)
# canvas.pack()

# # Run the window loop
# ventana.mainloop()